import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props}
import akka.stream.ActorMaterializer
import org.openapitools.client.api.{DefaultApi, EnumsSerializers}
import org.openapitools.client.core.{ApiInvoker, ApiResponse}
import akka.pattern.{ask, pipe}
import org.openapitools.client.model.Vente

import scala.util.{Failure, Success}

trait Parameters {
  val length = 10000 /* number of calls per client (calls are executed in sequence) */
  val width = 100 /* number of clients running in parallel */
  /* bench :
    Length x width
    1000 x 1000 -> pool overflow
    10000 x 100 -> 1min 04s (all 8 CPUs running high)
    100000 x 10 -> 3min 16s (all 8 CPUs running high)
    1000000 x 1 -> 4 min 24s (all 8 CPUs running at 30%-60%)
  */
}

object ApiInvokerActor {
  case object GetStats
  case class PostClients(nom: String, nombre: Int)
  def apply(): Props = Props(new ApiInvokerActor)
}

class ApiInvokerActor extends Actor with ActorLogging {
  implicit val system = context.system
  import system.dispatcher // The ExecutionContext that will be used
  val apidefault = DefaultApi()
  val invoker: ApiInvoker = ApiInvoker(EnumsSerializers.all)
  import ApiInvokerActor._

  override def receive: Receive = {
    case GetStats =>
      val replyto = sender
      val invocation = invoker.execute(apidefault.livraisonsStatsGet())
      invocation.pipeTo(replyto)

    case PostClients(conso, nombre) =>
      val replyto = sender
      val invocation = invoker.execute(apidefault.clientsConsommationNombrePost(conso, nombre))
      invocation.pipeTo(replyto)
  }
}

object Client {
  def apply(invoker: ActorRef, conso: String): Props = Props(new Client(invoker, conso))
}

class Client(invoker: ActorRef, conso: String) extends Actor with ActorLogging with Parameters {
  var total = length
  import ApiInvokerActor._

  def askOneMoreDrink = invoker ! PostClients(conso, 1)

  override def receive: Receive = {

    case ApiResponse(200, Unit, _) =>
      total -= 1
      if (total > 0) askOneMoreDrink
      else {
        log.info(s"total : $total")
        invoker ! GetStats
      }

    case ApiResponse(200, xs: List[Vente], _) => log.info(s"Stats: $xs")

    case ApiResponse(n, d, _) => log.info(s"HTTP $n received with $d")

    case msg @ _ => log.info(s"Unexptected message received: ${msg}")
  }

  override def preStart(): Unit = log.info("Lets roll!"); askOneMoreDrink
}

object Main extends App with Parameters {

  // needed to run the route
  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()
  // needed for the future map/flatmap in the end and future in fetchItem and saveOrder
  implicit val executionContext = system.dispatcher

  val apiinvoker = system.actorOf(ApiInvokerActor(), "apiinvoker")
  (1 to width).foreach { (i) =>
    system.actorOf(Client(apiinvoker,s"conso_$i"),s"client_$i")
  }
}