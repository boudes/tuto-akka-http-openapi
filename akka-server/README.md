# Api tuto Akka-http

Une API simple pour le tuto akka-http


    ## API

          ### Default

          |Name|Role|
          |----|----|
          |`org.openapitools.server.api.DefaultController`|akka-http API controller|
          |`org.openapitools.server.api.DefaultApi`|Representing trait|
              |`org.openapitools.server.api.DefaultApiImpl`|Default implementation|

                * `POST /api/clients/{consommation}/{nombre}` - créer des clients
                * `POST /api/clients` - créer des clients
                * `GET /api/livraisons/stats/{consommation}` - Statistiques des ventes d&#39;une consommation
                * `GET /api/livraisons/stats` - Statistiques des ventes

