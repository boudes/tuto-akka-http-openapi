package io.swagger.server.api

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.unmarshalling.FromRequestUnmarshaller
import akka.http.scaladsl.marshalling.ToEntityMarshaller
import io.swagger.server.AkkaHttpHelper._
import io.swagger.server.model.CreerClients
import io.swagger.server.model.Vente
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.server.{Directives, Route}
import akka.stream.ActorMaterializer
import spray.json.{DefaultJsonProtocol, JsObject, RootJsonFormat}
import akka.http.scaladsl.unmarshalling._
import spray.json.DefaultJsonProtocol.jsonFormat2


class DefaultApi(
    defaultService: DefaultApiService,
    defaultMarshaller: DefaultApiMarshaller
) extends SprayJsonSupport {
  import defaultMarshaller._


  lazy val route: Route =
    path("clients" / Segment / IntNumber) { (consommation, nombre) =>
      post {





                  defaultService.clientsConsommationNombrePost(consommation = consommation, nombre = nombre)





      }
    } ~
    path("clients") {
      post {




                entity(as[CreerClients]){ body =>
                  defaultService.creerClients(body = body)
                }




      }
    } ~
    path("livraisons" / "stats" / Segment) { (consommation) =>
      get {





                  defaultService.livraisonsStatsConsommationGet(consommation = consommation)





      }
    } ~
    path("livraisons" / "stats") {
      get {





                  defaultService.livraisonsStatsGet()





      }
    }
}

trait DefaultApiService {

  def clientsConsommationNombrePost200: Route =
    complete((200, "Clients créés."))
  def clientsConsommationNombrePost400: Route =
    complete((400, "Les clients n&#39;ont pas été créés."))
  /**
   * Code: 200, Message: Clients créés.
   * Code: 400, Message: Les clients n&#39;ont pas été créés.
   */
  def clientsConsommationNombrePost(consommation: String, nombre: Int): Route

  def creerClients200: Route =
    complete((200, "Clients créés."))
  def creerClients400: Route =
    complete((400, "Les clients n&#39;ont pas été créés."))
  /**
   * Code: 200, Message: Clients créés.
   * Code: 400, Message: Les clients n&#39;ont pas été créés.
   */
  def creerClients(body: CreerClients): Route

  def livraisonsStatsConsommationGet200(responseVente: Vente)(implicit toEntityMarshallerVente: ToEntityMarshaller[Vente]): Route =
    complete((200, responseVente))
  /**
   * Code: 200, Message: Le nom de la consommation, la quantité et le produit de ses ventes, DataType: Vente
   */
  def livraisonsStatsConsommationGet(consommation: String)
      (implicit toEntityMarshallerVente: ToEntityMarshaller[Vente]): Route

  def livraisonsStatsGet200(responseVentearray: List[Vente])(implicit toEntityMarshallerVentearray: ToEntityMarshaller[List[Vente]]): Route =
    complete((200, responseVentearray))
  /**
   * Code: 200, Message: Une liste de noms de consommations servies avec la quantité et le produit des ventes, DataType: List[Vente]
   */
  def livraisonsStatsGet()
      (implicit toEntityMarshallerVentearray: ToEntityMarshaller[List[Vente]]): Route

}

trait DefaultApiMarshaller {
// implicit def fromRequestUnmarshallerCreerClients: FromRequestUnmarshaller[CreerClients]
// using this previous implicit instead of the following will crash at runtime !

  implicit def creerClientsFormat: RootJsonFormat[CreerClients]

  implicit def toEntityMarshallerVente: ToEntityMarshaller[Vente]

  implicit def toEntityMarshallerVentearray: ToEntityMarshaller[List[Vente]]

}
