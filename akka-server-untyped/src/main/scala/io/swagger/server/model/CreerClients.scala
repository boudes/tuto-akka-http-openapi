package io.swagger.server.model


/**
 * @param consommation 
 * @param nombre 
 */
case class CreerClients (
  consommation: String,
  nombre: Int
)

