package fr.mipn.bar

import akka.actor.{ Props, ActorSystem, Actor, ActorLogging, ActorRef, PoisonPill }
import io.swagger.server.model._

/*
Diagramme de séquence du bar

Client                  Serveur                           Barman                Bar
  |                        |                               |                     |
  |--------------------------------Bonjour-------------------------------------->|
  |<------------------VotreServeur(s)--------------------------------------------|
  |                        |                               |                     |
  |----Commande("café")--->|                               |                     |
  |                        |--CommandeClient("café", c)--->|                     |
  |                        |<-LivraisonCl("café", c, 150)--|                     |
  |                        |                               |                     |
  |<-Livraison("café",150)-|                               |                     |
  |----------------------Livraison("café", 150)------pour les stats------------->|
  |--Payer(150)----------->|                               |                     |


 API                               Bar
  |                                 |
  |------------Stat("café")-------->|
  |<-----Vente("café", 1, 150)------|
  |                                 |
  |------------Stats--------------->|
  |<--List(Vente("café", 1, 150))---|
  |                                 |
  |---CreerClients("café", 100)---->|
  |<--------------true--------------|
 */

/* Un Barman
   connait : rien
   reçoit : CommandeClient(un nom de conso, actorref du client)
   au démarrage : rien
   à réception de CommandeClient(conso, client) de la part d'un serveur, trouve un prix pour la conso et répond au serveur LivraisonClient(conso, client, prix)
 */

object Barman {
  case class CommandeClient(conso: String, client: ActorRef)
  var carte = Map("café" -> 150, "jus" -> 300, "thé" -> 200)
  def apply(): Props = Props(new Barman())
}

class Barman extends Actor with ActorLogging {
  import Serveur.LivraisonClient
  import Barman._
  def receive: Receive = {
    case CommandeClient(conso, client) =>
      val prix = carte.getOrElse(conso, 500)
      sender ! LivraisonClient(conso, client, prix)
    case msg @ _ => log.info(s"j'ai reçu le message : $msg")
  }
}

/* Un client
   connait : le actorref du bar et le nom de sa conso (commande)
   reçoit :
   - VotreServeur(un actorref de serveur)
   - Livraison(nom de conso, prix de la conso)
   au démarrage : dit bonjour au bar
   à réception de :
   - VotreServeur(un actorref de serveur), passe commande d'une conso auprès du serveur
   - Livraison(nom de conso, prix de la conso), réponds avec Payer(prix de la conso)
*/

object Client {
  case class VotreServeur(serveur: ActorRef)
  case class Livraison(commande: String, prix: Int)
  def apply(commande: String, bar: ActorRef): Props = Props(new Client(commande, bar))
}

class Client(commande: String, bar: ActorRef) extends Actor with ActorLogging {
  import Serveur._
  import Bar.Bonjour
  import Client._

  def receive: Receive = {
    case VotreServeur(serveur) => serveur ! Commande(commande)
    case msg @ Livraison(nom, prix) =>
      bar ! msg
      sender ! Payer(prix)
      // log.info(s"Bon ${nom} !")
    case msg @ _ => log.info(s"j'ai reçu le message : $msg")
  }

  override def preStart(): Unit = {
    bar ! Bonjour
  }
}

/* Un serveur
   connait : le barman
   reçoit :
   - Commande(un nom de conso)
   - LivraisonClient(nom de conso, actorref du client, prix de la conso)
   - Payer(prix)
   au démarrage : rien
   à réception de :
   - Commande(conso) de la part d'un client, envoie CommandeClient(conso, client)
     au barman
   - LivraisonClient(conso, client, prix), envoie Livraison(conso, prix) au client
   - Payer(prix) tue le client
 */

object Serveur {
  case class Commande(nom: String)
  case class LivraisonClient(nom: String, client: ActorRef, prix: Int)
  case class Payer(prix: Int)
  def apply(barman: ActorRef): Props = Props(new Serveur(barman))
}

class Serveur(barman: ActorRef) extends Actor with ActorLogging {
  import Barman.CommandeClient
  import Client.Livraison
  import Serveur._
  var caisse = 0

  def receive: Receive = {
    case Commande(nom) =>
      val client = sender
      barman ! CommandeClient(nom, client)
    case LivraisonClient(nom, client, prix) => client ! Livraison(nom, prix)
    case Payer(prix) =>
      val client = sender
      caisse += prix
      client ! PoisonPill
    case msg @ _ => log.info(s"j'ai reçu le message : $msg")
  }
}

/*
Le bar
connaît : le actorref du barman
reçoit : Bonjour, livraison
au démarrage : crée des serveurs et des barmans
à réception de :
- Bonjour répond VotreServeur(un actorref de serveur)
- Livraison enregistre la livraison
*/

object Bar {
  case object Bonjour
  case class Stat(consommation: String)
  case object Stats
  def apply(barman: ActorRef): Props = Props(new Bar(barman))
  var stats: Map[String,(Int, Int)] = Map.empty
}

class Bar(barman: ActorRef) extends Actor with ActorLogging {
  import scala.util.Random
  import Bar._
  import io.swagger.server.model.CreerClients
  import Client.Livraison

  var serveurs: Vector[ActorRef] = Vector.empty
  import Client.VotreServeur
  def get_server = serveurs(Random.nextInt(serveurs.size))
  def receive: Receive = {
    case Bonjour => if (serveurs.size > 0) sender ! VotreServeur(get_server)

    case CreerClients(consommation, nombre) =>  {
      (1 to nombre).map((i) => context.actorOf(Client(consommation, self)))
      sender ! true
    }

    case msg @ Livraison(nom, prix) => {
      val (quantité, produit) = stats.getOrElse(nom,(0,0))
      stats = stats ++ List((nom,(quantité + 1, produit + prix)))
    }

    case Stat(conso) => {
      val (quantité, produit) = stats.getOrElse(conso, (0,0))
      sender ! Vente(conso, quantité, produit)
    }

    case Stats => {
      val ventes = stats.toList.map{
        case (consommation, (quantité, produit)) =>
          Vente(consommation, quantité, produit)
      }
      sender ! ventes
    }

    case msg @ _ => log.info(s"j'ai reçu le message : $msg")
  }

  override def preStart(): Unit = {
    serveurs = (for { i <- 1 to 3 } yield {
      context.actorOf(Serveur(barman), s"serveur${i}")
    }).toVector
  }
}


object ApiMessages {
  case class Vente(consommation: String, quantité: Int, produit: Int)
}

class BarDesActeurs(system: ActorSystem) {
  val barman = system.actorOf(Barman(), name = "lebarman")
  val bar = system.actorOf(Bar(barman), name = "lebar")
  val serveurweb = system.actorOf(Serveur(barman), name = "serveurweb")
  val quelques_clients = (1 to 10).map((i) => system.actorOf(Client("café", bar)))
}
