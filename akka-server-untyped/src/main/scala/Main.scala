import akka.actor.{ActorLogging, ActorSystem}
import akka.http.scaladsl.Http
import akka.Done
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.model.StatusCodes
import spray.json.RootJsonFormat
// for JSON serialization/deserialization following dependency is required:
// "com.typesafe.akka" %% "akka-http-spray-json" % "10.1.7"
import akka.http.scaladsl.unmarshalling.Unmarshaller
import akka.http.scaladsl.marshalling.ToEntityMarshaller
import akka.http.scaladsl.unmarshalling.FromRequestUnmarshaller
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.stream.ActorMaterializer
import spray.json.{DefaultJsonProtocol, JsObject}


import scala.io.StdIn

import scala.concurrent.Future

import io.swagger.server.api._
import io.swagger.server.model._

import akka.http.scaladsl.Http.ServerBinding

import fr.mipn.bar.BarDesActeurs

import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.language.postfixOps


object Main extends App {

  // needed to run the route
  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()
  // needed for the future map/flatmap in the end and future in fetchItem and saveOrder
  implicit val executionContext = system.dispatcher

  val lebar = new BarDesActeurs(system)

  object DefaultMarshaller extends DefaultApiMarshaller with SprayJsonSupport {
    import DefaultJsonProtocol._

    implicit def creerClientsFormat: RootJsonFormat[CreerClients] = jsonFormat2(CreerClients)

    /* cleaner alternative but useless (crash at runtime) */
    def toRequestUm(implicit um : FromRequestUnmarshaller[CreerClients]) = um

    implicit def fromRequestUnmarshallerCreerClients: FromRequestUnmarshaller[CreerClients] = toRequestUm
    /* end of useless definitions */

    implicit val toEntityMarshallerVente: ToEntityMarshaller[Vente] = jsonFormat3(Vente)

    implicit val toEntityMarshallerVentearray: ToEntityMarshaller[List[Vente]] = listFormat(jsonFormat3(Vente))

  }

  object DefaultService extends DefaultApiService {
    import fr.mipn.bar.Bar._
    implicit val timeout = new Timeout(2 seconds)

    def livraisonsStatsConsommationGet(consommation: String)
      (implicit toEntityMarshallerVente: ToEntityMarshaller[Vente]): Route = {
      /* Plutôt que de répondre directement comme ceci :
       livraisonsStatsConsommationGet200(Vente(consommation, 2, 500))
       On simule une réponse du future */
      val reponse = (lebar.bar ? Stat(consommation)).mapTo[Vente]

      /* Route  est un alias de type :
       type Route = RequestContext => Future[RouteResult]
       Si on souhaite réutiliser les réponses toutes faites préparées par swagger dans
       le trait DefaultApiService en y insérant des valeurs venant du future
       (typiquement la réponse au message d'un acteur) alors il faut composer
       la future valeur avec le Future[RouteResult] à l'aide de flatMap à l'intérieur
       de la flèche (on utilise une abstraction sur la RequestContext).
       */

      requestcontext => {
        (reponse).flatMap {
          (vente: Vente) =>
          livraisonsStatsConsommationGet200(vente)(toEntityMarshallerVente)(requestcontext)
        }
      }

      /* Alternative :
       On peut également ne pas utiliser les réponses toutes prêtes proposées dans le code
       généré par swagger, et manipuler directement les Futures.

       onSuccess(reponse){(vente:Vente) => complete((200,vente))}
       */
    }

    def clientsConsommationNombrePost(consommation: String, nombre: Int) = {

      val reponse = (lebar.bar ? CreerClients(consommation, nombre)).mapTo[Boolean]

      requestcontext => {
        (reponse).flatMap {
          (succes: Boolean) =>
          if (succes) creerClients200(requestcontext)
          else creerClients400(requestcontext)
        }
      }
    }

    def creerClients(msg: CreerClients) = {

      val reponse = (lebar.bar ? msg).mapTo[Boolean]

      requestcontext => {
        (reponse).flatMap {
          (succes: Boolean) =>
          if (succes) clientsConsommationNombrePost200(requestcontext)
          else clientsConsommationNombrePost400(requestcontext)
        }
      }
    }

    def livraisonsStatsGet()(implicit toEntityMarshallerVentearray: ToEntityMarshaller[List[Vente]]) = {
      val reponse = (lebar.bar ? Stats).mapTo[List[Vente]]

      requestcontext => {
        (reponse).flatMap {
          (ventes: List[Vente]) => livraisonsStatsGet200(ventes)(toEntityMarshallerVentearray)(requestcontext)
        }
      }
    }
  }

  val api = new DefaultApi(DefaultService, DefaultMarshaller)

  val host = "localhost"
  val port = 8080

  val bindingFuture = Http().bindAndHandle(pathPrefix("api"){api.route}, host, port)
  println(s"Server online at http://${host}:${port}/\nPress RETURN to stop...")

  bindingFuture.failed.foreach { ex =>
    println(s"${ex} Failed to bind to ${host}:${port}!")
  }

  StdIn.readLine() // let it run until user presses return
  bindingFuture
    .flatMap(_.unbind()) // trigger unbinding from the port
    .onComplete(_ => system.terminate()) // and shutdown when done
}
